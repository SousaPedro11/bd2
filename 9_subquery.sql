create schema if not exists subquery;
use subquery;

create table cargos(
id int primary key,
descricao varchar(45),
nivel int
);

create table departamentos(
id int primary key,
nome varchar(45)
);

create table funcionarios(
id int primary key,
nome varchar(45),
salario decimal(10,2),
cargo_id int,
departamento_id int,
foreign key (cargo_id) references cargos(id),
foreign key (departamento_id) references departamentos(id)
);

insert into cargos values (1, 'Analista Junior', 1);
insert into cargos values (2, 'Analista Junior', 2);
insert into cargos values (3, 'Analista Junior', 3);
insert into cargos values (4, 'Analista Pleno', 1);
insert into cargos values (5, 'Analista Pleno', 2);
insert into cargos values (6, 'Analista Pleno', 3);	
insert into cargos values (7, 'Analista Senior', 1);
insert into cargos values (8, 'Analista Senior', 2);
insert into cargos values (9, 'Analista Senior', 3);	
insert into cargos values (10, 'DBA', 1);
insert into cargos values (11, 'DBA', 2);
insert into cargos values (12, 'DBA', 3);
insert into cargos values (13, 'secretaria', 1);	

insert into departamentos values (1, 'administrativo');
insert into departamentos values (2, 'ti');
	
	
insert into funcionarios values (1, 'Maria Silva', 800.00, 13,1);			
insert into funcionarios values (2, 'Mario Carvalho', 3000.00, 2,2);
insert into funcionarios values (3, 'Felipe Fernandes', 12000.00, 11, 2);
insert into funcionarios values (4, 'Vitor Cunha', 8000.00, 9, 2);				

-- 1
select f.nome as funcionario, c.descricao as cargo, d.nome as departamento from
 funcionarios f
inner join cargos c on f.cargo_id = c.id
inner join departamentos d on f.departamento_id = d.id
and
 f.salario = (select max(f2.salario) from funcionarios f2)
;

-- 2
select f.nome, f.salario as funcionario from
 funcionarios f
inner join departamentos d on f.departamento_id = d.id
and
 f.salario > (
     select avg(f2.salario) as media from
      funcionarios f2
     inner join departamentos d2 on f2.departamento_id = d2.id
                    and
      d2.nome like 'ti'
    )
;

-- 3
select f.nome as funcionario, c.descricao as cargo, c.nivel as nivel_cargo from
 funcionarios f
inner join cargos c on f.cargo_id = c.id
and
 c.nivel = (
    select c2.nivel from
     funcionarios f2
    inner join cargos c2 on f2.cargo_id = c2.id
    and
     f2.nome like 'mario%'
)
;