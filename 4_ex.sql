drop schema joins;
create schema joins;
use joins;

CREATE TABLE usuarios(
	id INT PRIMARY KEY,
	nome VARCHAR(45),
	id_turma INT
);
CREATE TABLE turmas(
	id INT PRIMARY KEY,
	sigla VARCHAR(5)
);

INSERT INTO turmas VALUES (1,'SI3NA');
INSERT INTO turmas VALUES (2,'SI4NA');
INSERT INTO turmas VALUES (3,'SI5NA');
INSERT INTO turmas VALUES (4,'SI6NA');

INSERT INTO usuarios VALUES (1,'Manoel',1);
INSERT INTO usuarios VALUES (2,'Maria',1);
INSERT INTO usuarios VALUES (3,'Mario',1);
INSERT INTO usuarios VALUES (4,'Pedro',2);
INSERT INTO usuarios VALUES (5,'Felipe',2);
INSERT INTO usuarios VALUES (6,'Rodolfo',2);
INSERT INTO usuarios VALUES (7,'Clivia',3);
INSERT INTO usuarios VALUES (8,'Marcos',3);
INSERT INTO usuarios VALUES (9,'Venicios',3);
INSERT INTO usuarios VALUES (10,'Samuel',0);

select usuarios.nome, turmas.sigla from
	usuarios
inner join turmas on turmas.id = usuarios.id_turma and usuarios.nome like '%a'
;

select usuarios.nome, turmas.sigla from
	usuarios
inner join turmas on turmas.id = usuarios.id_turma
	and
		usuarios.id in (1, 3, 5, 7)
;

select usuarios.nome from
	usuarios
inner join turmas on turmas.id = usuarios.id_turma
	and
		turmas.sigla like 'si3na'
order by usuarios.nome desc
;

select usuarios.* from
	usuarios
inner join turmas on turmas.id = usuarios.id_turma
	and
		turmas.sigla like '%5%'
order by usuarios.id desc
;