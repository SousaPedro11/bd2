-- DDL -------------------------------------------------------------------
# APAGA A BASE CASO EXISTA
DROP DATABASE IF EXISTS hospital;

# CRIA A BASE
CREATE DATABASE IF NOT EXISTS hospital
DEFAULT CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci;

# SELECIONA A BASE
USE hospital;

# CRIACAO DAS TABELAS
CREATE TABLE IF NOT EXISTS medico(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    endereco VARCHAR(80) NOT NULL,
    telefone VARCHAR(45) NOT NULL,
    crm VARCHAR(45) NOT NULL
) DEFAULT CHARSET utf8mb4;

CREATE TABLE IF NOT EXISTS paciente(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    endereco VARCHAR(80) NOT NULL,
    telefone VARCHAR(45) NOT NULL,
    cpf VARCHAR(45) NOT NULL
) DEFAULT CHARSET utf8mb4;

CREATE TABLE IF NOT EXISTS secretaria(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    endereco VARCHAR(80) NOT NULL,
    telefone VARCHAR(45) NOT NULL,
    cpf VARCHAR(45) NOT NULL
) DEFAULT CHARSET utf8mb4;

CREATE TABLE IF NOT EXISTS consulta(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    data_consulta DATE NOT NULL,
    medico_id INT NOT NULL,
    paciente_id INT NOT NULL,
    secretaria_id INT NOT NULL,
    valor DECIMAL(10,2) NOT NULL,
    pago BOOLEAN DEFAULT false
) DEFAULT CHARSET utf8mb4;

# DEFINICAO DE CONSTRAINTS
## FK
ALTER TABLE consulta
ADD CONSTRAINT FK_consulta_medico FOREIGN KEY (medico_id)
REFERENCES medico (id)
ON UPDATE no action
ON DELETE no action
;

ALTER TABLE consulta
ADD CONSTRAINT FK_consulta_paciente FOREIGN KEY (paciente_id)
REFERENCES paciente (id)
ON UPDATE no action
ON DELETE no action
;

ALTER TABLE consulta
ADD CONSTRAINT FK_consulta_secretaria FOREIGN KEY (secretaria_id)
REFERENCES secretaria (id)
ON UPDATE no action
ON DELETE no action
;

-- DML -------------------------------------------------------------------
# INSERTS
INSERT INTO hospital.medico
(nome, endereco, telefone, crm)
VALUES
('NILZA EMILIA SEABRA OLIVEIRA','TRAVESSA BARAO DO TRIUNFO 3260 - CLINICA PARICUIA','32424829','4773 - PA'),
('SERGIO LUIZ DOS SANTOS BRITO FILHO','TRAVESSA TRES DE MAIO 1218 - SALA 206 E 207','32497988','10010 - PA'),
('JOSE SILVERIO NUNES DA FONSECA','RUA DOS MUNDURUCUS 3100 - 1º ANDAR','32897755','4775 - PA'),
('JOSE SILVERIO NUNES DA FONSECA','TRAVESSA DOM ROMUALDO DE SEIXAS 1812','32154522','4775 - PA'),
('ANA PAULA DA SILVA LESSA','AVENIDA GOVERNADOR JOSE MALCHER 937 - SALA 704','32237674','9273 - PA')
;

INSERT INTO hospital.paciente
(nome, endereco, telefone, cpf)
VALUES
('MIGUEL SOUSA','ORQUIDEA 89','32530000','01234567890'),
('ALVARO BARROS','MAUTITI 245','32450000','23456789012'),
('PEDRO SOUSA','ORQUIDEA 89','32530000','52731312345'),
('PEDRO SOUSA','ORQUIDEA 89','32530000','23645012345'),
('ANTONIO BRAZAO','JUVENAL 245','32530102','01545012345')
;

INSERT INTO hospital.secretaria
(nome, endereco, telefone, cpf)
VALUES
('CLAUDIA MONTEIRO','RUA 1 80','32490000','78945612300'),
('CARLA PAIXAO','RUA 2 854','32980000','56123078945'),
('GLEICE OLIVEIRA','RUA 3 78','32530506','45612307894'),
('CAROL COELHO','ALAMEDA D COELHO 45','32530607','2307894561'),
('CARLA BREZZA','PASSAGEM VEMTIMBORA 157','32538954','89456123078')
;

INSERT INTO hospital.consulta
(data_consulta, medico_id, paciente_id, secretaria_id, valor, pago)
VALUES
('2019-09-09','1','1','1','150.50','1'),
('2019-10-16','5','1','2','450.65','1'),
('2019-12-12','4','2','5','120.00', '0'),
('2019-09-11','3','3','3','150.30','1'),
('2019-11-03','4','5','4','130.00','1'),
('2019-11-03','2','4','3','480.65','1')
;

-- DQL -------------------------------------------------------------------
SELECT
	p.nome AS 'Paciente',
	m.nome AS 'Médico',
    m.endereco AS 'Endereco do Consultório',
	c.data_consulta AS 'Data da Consulta',
	c.valor AS 'Valor da Consulta',
    if(c.pago, 'Sim', 'Não') AS 'Pago'
FROM
	hospital.consulta c,
    hospital.paciente p,
    hospital.medico m
WHERE
	c.paciente_id = p.id
AND
	c.medico_id = m.id
ORDER BY
	c.data_consulta
;