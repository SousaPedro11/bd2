-- create database dbname;
-- use dbname;
-- select database();
-- show databases;
-- drop database dbname;

-- create database teste;
-- use teste;
-- create table pessoas(
-- 	id int,
--     nome varchar(255),
--     endereco varchar(255),
--     bairro varchar(255)
-- );

-- describe pessoas;
-- desc pessoas;

insert into pessoas values (1, 'Mario', 'Rua', 'Marco');
insert into pessoas (id, nome, endereco, bairro) values (1, 'Mario', 'Rua', 'Marco');
insert into pessoas (id, nome, endereco, bairro) values (1, 'Mario', 'Rua', 'Marco');
insert into pessoas (nome, endereco, bairro) values ('Mario', 'Rua', 'Marco');

alter table pessoas modify nome varchar(25);
alter table pessoas change column nome_pessoa nome varchar(255);
alter table pessoas drop column bairro;
alter table pessoas add column bairro varchar(255);
desc pessoas;

create table pessoas1(
	id int,
    nome varchar(255) not null,
    endereco varchar(255),
    bairro varchar(255)
);

create table pessoas2(
	id int,
    nome varchar(255) not null unique,
    endereco varchar(255),
    bairro varchar(255)
);

desc pessoas1;
desc pessoas2;

alter table pessoas add unique (id);
alter table pessoas1 add unique (id);
alter table pessoas2 add unique (id);

alter table pessoas2 drop index id;

create table pessoas3(
	id int primary key,
    nome varchar(255),
    endereco varchar(255),
    bairro varchar(255)
);

insert into pessoas3 values	(NULL, 'Mario', 'Rua', 'Marco');
insert into pessoas3 values (1, 'Mario', 'Rua', 'Marco');
insert into pessoas3 values (1, 'Mario', 'Rua', 'Marco');

create table pessoas4(
	id int primary key auto_increment,
    nome varchar(255),
    endereco varchar(255),
    bairro varchar(255)
);

insert into pessoas4 values (NULL, 'Mario', 'Rua', 'Marco');
insert into pessoas4 values (NULL, 'Mario2', 'Rua2', 'Marco2');

alter table pessoas2 add primary key (id);
alter table pessoas2 drop primary key;

create table compras(
	id int primary key,
    cod int not null,
    pessoa_id int
);

alter table compras add constraint FK_compras_pessoa foreign key (pessoa_id) references pessoas4 (id);

insert into compras values
	(1,12,1),
    (2,12,2)
;

alter table compras drop foreign key FK_compras_pessoa;

drop table pessoas, compras;

drop database teste;