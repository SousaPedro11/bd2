drop schema if exists prova;
create schema prova;
use prova;

create table categorias(
	id integer primary key,
	nome varchar(45)
);

create table autores(
	id integer primary key,
	nome varchar(45)
);

create table livros(
	id integer primary key,
	titulo varchar(45),
	qtd integer,
	preco decimal(10,2),
	categoria_id integer,
	autor_id integer,
	foreign key (categoria_id) references categorias(id),
	foreign key (autor_id) references autores(id)
);


# inserts
insert into autores values (1, 'Jaime Alves');
insert into autores values (2, 'Marcos Trindade');
insert into autores values (3, 'Alexandre Martins');
insert into autores values (4, 'Fabio Diniz');
insert into autores values (5, 'Tereza Velasques');

insert into categorias values (1, 'Programação');
insert into categorias values (2, 'Redes');
insert into categorias values (3, 'Banco de Dados');
insert into categorias values (4, 'Inteligência Artificial');
insert into categorias values (5, 'Engenharia de Software');

insert into livros values (1, 'Java 21 dias', 10, 30.9,1, 1);
insert into livros values (2, 'Ruby', 13, 32.5, 1, 3);
insert into livros values (3, 'Python', 35, 40.99,1, 5);
insert into livros values (4, 'Erlang', 5, 99.99, 1, 4);
insert into livros values (5, 'Java como Programar', 30, 12.90, 1, 4);
insert into livros values (6, 'Java para Android', 23, 50.75, 1, 3);
insert into livros values (7, 'Swift', 22, 89.99, 1, 3);
insert into livros values (8, 'Teste Unitário', 8, 45.99, 5, 1);
insert into livros values (9, 'RUP', 20, 49.99, 5, 2);
insert into livros values (10, 'Telecomunicações', 8, 39.99, 2, 1);
insert into livros values (11, 'SQL', 10, 29.99, 3, 1);
insert into livros values (12, 'MySQL', 23, 19.99, 3, 4);
insert into livros values (13, 'SQLite3', 26, 19.99, 3, 5);
insert into livros values (14, 'Ruby para programadores Java', 9, 39.99, 1, 2);
insert into livros values (15, 'Ansi C', 44, 19.99, 1, 2);

-- 1
select a.nome, c.nome from
 autores a
inner join livros l on l.autor_id = a.id and l.titulo like 'ruby'
inner join categorias c on l.categoria_id = c.id
;

-- 2
select a.nome, c.nome from
 autores a
inner join livros l on l.autor_id = a.id and l.titulo like '%21%'
inner join categorias c on l.categoria_id = c.id
;

-- 3
select a.nome, c.nome from
 autores a
inner join livros l on l.autor_id = a.id and l.titulo like '%java%'
inner join categorias c on l.categoria_id = c.id
;

-- 4
select a.nome, c.nome from
 autores a
inner join livros l on l.autor_id = a.id and l.titulo like 'E%'
inner join categorias c on l.categoria_id = c.id
;

-- 5
select au.nome, cat.nome from
 livros lv, autores au, categorias cat
where
 lv.qtd = (
    select l.qtd from
     autores a, livros l, categorias c
    where
     a.id = l.autor_id
    and
     c.id = l.categoria_id
    group by
     l.qtd
    order by 
     l.qtd
    desc
    limit
     1
   )
and
 lv.categoria_id = cat.id
and
 lv.autor_id = au.id
;

-- 6
select au.nome, cat.nome from
 livros lv, autores au, categorias cat
where
 lv.preco = (
    select l.preco from
     autores a, livros l, categorias c
    where
     a.id = l.autor_id
    and
     c.id = l.categoria_id
    group by
     l.preco
    order by 
     l.preco
    limit
     1
   )
and
 lv.categoria_id = cat.id
and
 lv.autor_id = au.id
;

-- 7
select c.nome, avg(l.preco) from
 livros l, categorias c
where
 c.id = l.categoria_id
group by c.nome
;

-- 8
select c.nome, sum(l.qtd) from
 livros l, categorias c
where
 c.id = l.categoria_id
group by c.nome
;

-- 9
select au.nome, lv.titulo, lv.preco from
 autores au, livros lv
where
 au.id = lv.autor_id
and
 lv.preco in (
select max(l.preco) from
 autores a, livros l
where
 a.id = l.autor_id
group by a.nome)
;

-- 10
select a.nome, sum(l.preco) from
autores a, livros l
where
a.id = l.autor_id
group by
a.nome
;