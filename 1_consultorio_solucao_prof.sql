create schema consultorio;

use consultorio;

create table medico (
	id int primary key auto_increment,
    nome varchar(45),
    endereco varchar(45),
    telefone varchar(45),
    crm varchar(45)
);

create table paciente(
	id int primary key auto_increment,
    nome varchar(45),
    endereco varchar(45),
    tel varchar(45),
    cpf varchar(45)
);

create table secretaria(
	id int primary key auto_increment,
    nome varchar(45),
    endereco varchar(45),
    telefone varchar(45),
    cpf varchar(45)
);

create table consulta(
	id int primary key auto_increment,
    data_cons date,
    medico_id int,
    foreign key (medico_id) references medico(id),
    paciente_id int,
    foreign key (paciente_id) references paciente(id),
    secretaria_id int,
    foreign key (secretaria_id) references secretaria(id),
    valor decimal(10,2),
    pago bool
); 


insert into medico values(null,'Sales','Av AAAA','99999-9999','0002'); 
insert into medico values(null,'Mario','Av bbb','99999-9999','0003'); 
insert into medico values(null,'Antonio','Av cccc','99999-9999','0010'); 
insert into medico values(null,'Campos','Av ddd','99999-9999','0015'); 
insert into medico values(null,'Jose','Av www','99999-9999','0020'); 


insert into paciente values(null,'AAAA','Av AAAA','00000-0000','11111111111'); 
insert into paciente values(null,'BBBB','Av BBBB','00000-0000','22222222222');
insert into paciente values(null,'CCCC','Av CCCC','00000-0000','33333333333');
insert into paciente values(null,'DDDD','Av DDDD','00000-0000','44444444444');
insert into paciente values(null,'EEEE','Av EEEE','00000-0000','55555555555');

insert into secretaria values(null,'sec1','Av AAAA','00000-0000','11111111111'); 
insert into secretaria values(null,'sec2','Av BBBB','00000-0000','22222222222');
insert into secretaria values(null,'sec3','Av CCCC','00000-0000','33333333333');
insert into secretaria values(null,'sec4','Av DDDD','00000-0000','44444444444');
insert into secretaria values(null,'sec5','Av EEEE','00000-0000','55555555555');

insert into consulta values(null, '2017-03-14', 1, 2, 3, 100.00, true);
insert into consulta values(null, '2017-03-15', 5, 3, 4, 120.00, true); 
insert into consulta values(null, '2017-03-16', 2, 4, 5, 150.00, true); 
insert into consulta values(null, '2017-03-17', 4, 5, 2, 180.00, true); 
insert into consulta values(null, '2017-03-20', 3, 1, 1, 200.00, true);  