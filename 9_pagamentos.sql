CREATE TABLE pagamentos (
	id INT PRIMARY KEY,
	cliente_id INT,
	modo_pagamento VARCHAR(20),
	valor decimal(9,2))
;
CREATE TABLE clientes (
	id INT PRIMARY KEY,
	nome VARCHAR(20))
;
INSERT INTO clientes VALUES (1, 'Joao');
INSERT INTO clientes VALUES (2, 'Maria');
INSERT INTO pagamentos VALUES (1, 1, 'cheque', 10.00);
INSERT INTO pagamentos VALUES (2, 1, 'debito', 100.00);
