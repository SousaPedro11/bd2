use joins;
select usuarios.nome, turmas.sigla from
	usuarios, turmas
;

-- INNER
select usuarios.nome, turmas.sigla from
	usuarios
inner join turmas on usuarios.id_turma = turmas.id
;

use joins;
select usuarios.nome, turmas.sigla from
	usuarios, turmas
where
	usuarios.id_turma = turmas.id
;

-- LEFT
select usuarios.nome, turmas.sigla from
	usuarios
left join turmas on usuarios.id_turma = turmas.id
;

-- RIGHT
select usuarios.nome, turmas.sigla from
	usuarios
right join turmas on usuarios.id_turma = turmas.id
;

-- OUTER
select usuarios.nome, turmas.sigla from
	usuarios
left join turmas on usuarios.id_turma = turmas.id
union
select usuarios.nome, turmas.sigla from
	usuarios
right join turmas on usuarios.id_turma = turmas.id
;