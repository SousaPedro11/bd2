drop schema if exists nap2;
create schema nap2;
use nap2;

create table alunos(
	id integer primary key,
	nome varchar(45)
);	

create table disciplinas(
	id integer primary key,
	nome varchar(45)
);

create table notas_freq(
	aluno_id int,
	disciplina_id int,
	nap1 decimal(4,2),
	nap2 decimal(4,2),
	freq double,
	primary key(aluno_id, disciplina_id),
	foreign key(aluno_id) references alunos(id),
	foreign key(disciplina_id) references disciplinas(id)
);

insert into alunos values (1, 'Mario Castro');
insert into alunos values (2, 'Felipe Silva');
insert into alunos values (3, 'Antonio Cardoso');
insert into alunos values (4, 'Maria Lima');			
insert into alunos values (5, 'Ana Castro');
insert into alunos values (6, 'Katia Barbosa');
insert into alunos values (7, 'Felipe Cardoso');
insert into alunos values (8, 'Teresa Lima');			
insert into alunos values (9, 'Alexandre Silva');
insert into alunos values (10, 'Wladson Pinto');

insert into disciplinas values(1, 'banco de dados');
insert into disciplinas values(2, 'redes');
insert into disciplinas values(3, 'programação');	
insert into disciplinas values(4, 'sistemas operacionais');	
insert into disciplinas values(5, 'linguagens formais');	

insert into notas_freq values (1, 1, 0, 7.5, 0.85);
insert into notas_freq values (2, 1, 4.5, 9.5, 0.85);
insert into notas_freq values (3, 1, 0, 0, 0.8);
insert into notas_freq values (4, 1, 4.5, 0, 0);     
insert into notas_freq values (5, 1, 6.5, 2, 0.7);
insert into notas_freq values (6, 1, 7, 5, 0.85);
insert into notas_freq values (7, 1, 0.5, 9.5, 0.55);
insert into notas_freq values (8, 1, 1.5, 1, 0.5); 
insert into notas_freq values (9, 1, 9.5, 1.5, 0.15);
insert into notas_freq values (10, 1, 1, 6, 0.05);
insert into notas_freq values (1, 2, 1.5, 3, 0.35); 
insert into notas_freq values (2, 2, 0, 10, 0.1); 
insert into notas_freq values (3, 2, 7, 6.5, 0.1);
insert into notas_freq values (4, 2, 7.5, 4.5, 0.3);
insert into notas_freq values (5, 2, 9, 6, 0.85);
insert into notas_freq values (6, 2, 4.5, 3, 1);  
insert into notas_freq values (7, 2, 4, 7.5, 0.45);
insert into notas_freq values (8, 2, 6, 9.5, 0.2);
insert into notas_freq values (9, 2, 8.5, 7, 1); 
insert into notas_freq values (10, 2, 8, 1.5, 0.65);
insert into notas_freq values (1, 4, 3, 9, 0.65);
insert into notas_freq values (2, 4, 1, 8, 0.9);  
insert into notas_freq values (3, 4, 6, 6, 0.75);
insert into notas_freq values (4, 4, 1, 0.5, 0.45);
insert into notas_freq values (5, 4, 8, 4, 0);  
insert into notas_freq values (6, 4, 3.5, 3.5, 0.55);
insert into notas_freq values (7, 4, 10, 7.5, 0.45);
insert into notas_freq values (8, 4, 9.5, 2, 0.6);
insert into notas_freq values (9, 4, 8.5, 3, 0.95);
insert into notas_freq values (10, 4, 3, 3.5, 0.25);
insert into notas_freq values (1, 5, 6.5, 3, 0.7);
insert into notas_freq values (2, 5, 0.5, 4, 0.65);
insert into notas_freq values (3, 5, 0.5, 7, 0); 
insert into notas_freq values (4, 5, 3.5, 9.5, 0.05);
insert into notas_freq values (5, 5, 3, 8, 0.25);
insert into notas_freq values (6, 5, 5.5, 2, 0.3);
insert into notas_freq values (7, 5, 3.5, 7.5, 1);
insert into notas_freq values (8, 5, 8.5, 6.5, 0.4);
insert into notas_freq values (9, 5, 6.5, 7, 0.35);
insert into notas_freq values (10, 5, 4, 2.5, 0.85);

-- 1
select a.nome, d.nome, nf.freq from
 notas_freq nf
inner join alunos a on nf.aluno_id = a.id and nf.freq < 0.75
inner join disciplinas d on nf.disciplina_id = d.id
order by a.nome
;

-- 2
-- caso a media se refira a todas as disciplinas do aluno
select a.nome, avg(nf.nap1) as media, avg(nf.freq) as frequencia from
 notas_freq nf
inner join alunos a on nf.aluno_id = a.id
inner join disciplinas d on nf.disciplina_id = d.id
group by a.nome
order by media desc
;

-- caso as nap1 sejam as medias
select a.nome, nf.nap1 as media, nf.freq as frequencia from
 notas_freq nf
inner join alunos a on nf.aluno_id = a.id
inner join disciplinas d on nf.disciplina_id = d.id
order by media desc
;

-- 3
select a.nome, min(nf.freq) from
 notas_freq nf
inner join alunos a on nf.aluno_id = a.id
group by a.nome
order by a.nome
;

-- 4
select nome as disciplina
from disciplinas
where id not in (select distinct disciplina_id 
from notas_freq);

-- 5
select a.nome, d.nome, nf.nap1, nf.nap2, nf.freq from
 notas_freq nf
inner join alunos a on nf.aluno_id = a.id
and
 nf.nap2 =
(
     select max(nf2.nap2) from
      notas_freq nf2
   )
inner join disciplinas d on nf.disciplina_id = d.id
;