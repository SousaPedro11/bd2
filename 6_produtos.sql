drop schema if exists funcoes;
create schema funcoes;
use funcoes;

CREATE TABLE produtos(
	id INT PRIMARY KEY,
	nome VARCHAR(45),
	valor DECIMAL(10,2)
);
		
INSERT INTO produtos VALUES (1,'TV',3500.00);
INSERT INTO produtos VALUES (2,'Geladeira',999.99);
INSERT INTO produtos VALUES (3,'Fogao',699.99);
INSERT INTO produtos VALUES (4,'Sofa',2000.10);
INSERT INTO produtos VALUES (5,'Mesa',700.00);
INSERT INTO produtos VALUES (6,'Cama',1599.99);
INSERT INTO produtos VALUES (7,'Armario',800.00);
INSERT INTO produtos VALUES (8,'Cadeiras',300.00);
INSERT INTO produtos VALUES (9,'TV',8000.00);

-- avg
select avg(valor) from produtos;

select nome, valor from
	produtos
where
	valor > (
			select avg(valor) from produtos
            )
;

-- count
select count(*) from produtos;
select count(*) as tamanho from produtos;
select count(distinct nome) from produtos;

-- first e last - limit
select * from produtos limit 1;
select * from produtos order by id desc limit 1;

-- min
select min(valor) from produtos;
select min(valor) as minimo from produtos;

-- max
select max(valor) from produtos;
select max(valor) as maximo from produtos;

-- sum
select sum(valor) from produtos;
select sum(valor) as soma from produtos;

-- group by
alter table produtos add tipo varchar(45);
update produtos set tipo = 'Eletronicos' where id in (1,9);
update produtos set tipo = 'Cozinha' where id in (2,3);
update produtos set tipo = 'Moveis' where id not in (1,2,3,9);

select count(*), tipo from produtos group by tipo;
select count(*) as qtd, tipo from produtos group by tipo;

-- having
select count(*) as qtd, tipo from produtos group by tipo having qtd > 4;