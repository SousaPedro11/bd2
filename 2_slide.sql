use consultorio;
select * from
	consulta;

select distinct medico_id, data_cons from consulta;

select * from
	medico
where
	nome = 'jose'
;

select * from
	consulta
where
	id = 3
;

select * from
	consulta
where
	id between 1 and 3
;

select * from
	consulta
where
	id not between 1 and 3
;

select * from
	medico
where
	nome like 'cam%'
;

select * from
	medico
where
	nome like '%m%'
;

select * from
	medico
where
	nome not like '%m%'
order by id desc
;

update medico set nome='Silas' where id = 1;